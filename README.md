Deliverables
============

## M1 DELIVERABLES

- [Functional Specification of VM Component](functional_specification_of_VM_component.md)
- [Interface Specification of VM Component](interface_specification_of_VM_component.md)
- [Envisioned Interoperability with Others](envisioned_interoperability_with_others.md)

## M9 DELIVERABLES

- [Feasibility Report ADVM](M9_feasibility_report_ADVM.md)
