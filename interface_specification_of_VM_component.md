# Interface Specification of VM Component

This section elaborates on the planned interfaces. All read interfaces will be designed as REST APIs and will be defined using OpenAPI Specification V3. All write operations will be designed in line with the latest Universal Registrar draft as two-stage operations (prepare, commit) via jsonrpc endpoint. Interfaces will be available in a form of a library or via endpoints.

In the table below, we present a preliminary list of interfaces. The list will be completed upon the finalization of the architecture and sequence diagrams.

| API | TYPE | DESCRIPTION |
| ------ | ------ | ------ |
| Request VM | REST | The holder can request a VM |
| Issue VM | REST | The issuer creates an unsigned VM |
| Get VM Schemas | REST | Obtain a list of all VM schemas |
| Register/update/revoke VM Schema | jsonrpc | Register/update/revoke a VM Schema |
| Request to share a VM | REST | Verifier requests a VM |
| Present VM | REST | Holder shares a VM |
| Verify VM | REST | Verifier Verifies a VM |
| Revoke VM | jsonrpc | Revoke a VM |
