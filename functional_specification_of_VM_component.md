# Functional Specification of VM Component

## Functional Perspective
This section contains the functional specifications wrt to the proposed project.

Note that this project will build on top of the existing Open Souce Software (OSS) libraries (walt.id), which have (partly) been funded by the European Commission via the 2nd ESSIFLab Infrastructure and will extend them, as well try to integrate/implement the flows and processes with existing SSI agents, i.e., NETIS’s AceBlock/AceID edge agent and University of Maribor’s SSI Enterprise agent.

In terms of the Trust Level Framework (as incubated by TrustOverIP), we will be focusing on the so-called Human-trust level or the upper two levels of governance and technology, as presented with Figure 1:

![Figure 1](fig1.PNG)<br>
**Figure 1: Focusing on the upper layers**

<br><br>The project will enable the following functionalities:

### SSI-specific functionality

| Concept | Enabled Functionality |
| ------ | ------ |
| Keys | The planned approach will avoid any operations related to changing controllership over keys. However, note that keys will obviously be required (e.g. for issuing and presenting VMs). |
| Decentralized Identifiers (DIDs) | Although the focus of the project is the extension of ESSIF towards verifiable delegation, the solution will be DID-method agnostic. |
| Verifiable Mandates (VM) <br> _Issuer / Verifiable Credentials (VCs)_ | Although the focus of the project is an extension of ESSIF towards verifiable delegation, we will try to be agnostic towards different VC “types” besides the EBSI’s V-IDs and VAs and the data exchange protocols (e.g. OIDC), e.g.: <br><br> -Define VM templates/schemas <br> -Import VM templates/schemas <br> -Configure VM templates/schemas and their respective delegation rules <br> -Create VMs <br> -Sign VMs (optional: eIDAS signatures) <br> -Issue VMs (OIDC/SIOPv2) <br> -Store VMs<br> -Manage VMs (lifecycle) <br> -VM Transfer |
| Verifiable Mandates (VM) <br> _Holder / Verifiable Presentations (VPs)_ | Although the focus of the project is an extension of ESSIF towards verifiable delegation, we will try to be agnostic towards different VC “types” besides the EBSI’s VPs and the data exchange protocols (e.g. OIDC), e.g.: <br><br> -Sign VMs (optional: eIDAS signatures) <br> -Present VMs (OIDC/SIOPv2) |
| Verifiable Mandates (VM) <br> _Verifier / Verifiable Presentations (VPs)_ | -Define VM verification policies <br> -Verify VMs (based on policies) |

### Registry-specific functionality

| Concept | Enabled Functionality |
| ------ | ------ |
| Trusted Registries (Optional) | Interactions with Trusted Registries: <br><br> -DID Registry <br> -Delegation/Mandate rule set registry Truster Issuer Registry (TIR) <br> -Trusted Accreditation Organization Registry (TAOR) <br> -Trusted Schema Registry (TSR) <br> -VC Status Registry |
| Business Logic / Flows (Optional) | We plan to build upon and validate the result based on the case study of business flows aligned with EBSI/ESSIF specifications (e.g. wrt issuance, verification), i.e., focused on public administration cases. |


## Architecture

Figure 2 illustrates a high-level architecture that consists of external services (EBSI, ID Hub), frameworks (EBSI/ESSIF, eIDAS) and protocols and an issuer/holder/verifier environment that consists of a wallet and supporting services. The high-level architecture design is technology agnostic. Below we present an instantiation of an architecture we will provide to demonstrate the capabilities and deliverables of this project:

- Issuer/Holder/Verifier environment: walt.id’s SSI Kit, HL Aries SSI Framework and other open-source libraries
- Frameworks: EBSI/ESSIF, eIDAS
- Protocols: OIDC for SSI

![Figure 2](fig2.PNG)<br>
**Figure 2: High-level architecture depiction of possible core components**

As from Fig 2. vivid, we plan to support various types of solutions and integrations, where the walt.id SSI Kit is the core either as

- standalone solution or
- “add-on” (for the seamless EBSI/ESSIF integration) in terms of integration with other frameworks (e.g. Aries). 

Figure 3 presents the details of the walt.id architecture. 

![Figure 3](fig3.PNG)<br>
**Figure 3: High-level walt.id architecture**

Note that the nature of VMs has certain “novel” implications on the implementation. Here are some examples: 

- Natural Persons (not only Legal Entities) will act as Issuers of VMs, i.e.:<br>
+Consumer Wallets must support this type of “issuing” as well as revoking functionalities and flows (backend, UI);<br>
+Custom “Issuer Portals”, which integrate SSI enterprise agents, are usually involved but are not necessarily required;<br>
+eIDAS-based implementations must support eSignatures (not only eSeals);<br>
+The delegation claims need to be somewhat standardized/machine-readable and modular.

- The verification process differs from “simple” use cases, e.g.:<br>
+It requires special verification policies suited for VMs;<br>
+The verification must include the scope of the VM as well as the transaction itself (to ensure that VM scopes are not exceeded).

## Components 

This section elaborations the proposed components based on the architecture diagram (above):

### VM Components (scope of this project)

| Component | Responsibility  | Used By |
| ------ | ------ | ------ |
| VC Lib<br>VM extension | -VC Specialization for VMs<br> -Provides data structures and methods | Library users (incl. Issuers, Holders, Verifiers) |
| Issuer (“Signatory”)<br>VM extension | -Provides functionality to issue VMs (to “Representatives”)<br> -Interfaces for integration | Library users for wallet implementation |
| Holder (“Custodian”)<br>VM extension | -Functionality to receive and hold VMs<br> -Interfaces for integration | Library users for wallet implementation |
| Verifier (“Auditor”)<br>VM extension | -Functionality to verify VMs<br> -Specialized VM verification policies<br> -Interfaces for integration | Verifiers |
| Wallet (Web UI)<br>VM extension | -Support for VM issuance<br> -Support for VM reception | Wallet Users |
| ESSIF / ecosystems | -Implement specialized work flows for issuance and presentation of VMs | Integrators for the respective ecosystem |

### 3rd party infrastructure by walt.id (out of scope; re-used for implementation/PoC)

| Component | Responsibility  | Used By |
| ------ | ------ | ------ |
| CLI Tool | -Command-line based user interface (UI)<br> -Runs entire functionality provided by the SSI Kit & Wallets<br> -Packaged as Docker container| Any library user, for testing, demo or for manual production use cases |
| SSI Kit<br>_(Signatory, Custodian, Auditor)_ | -Core SSI functions (wrt keys, DIDs, VCs)<br> -Interactions with Trust Registries (e.g. EBSI, DNS)<br> -Special business logic / flows (e.g. based on EBSI/ESSIF specifications<br> -Integration with 3rd party SSI components<br> -Configuration management | Internally used by the proposed VM components (see above) |
| Wallet<br>_(web-based wallet)_ | -Core consumer wallet functionality (wrt keys, DIDs, VCs) | Internally used by the proposed VM components (see above) |
| SSI Enterprise agent | -Core SSI functions (wrt keys, DIDs, VCs)<br> -Interactions with Trust Registries (e.g. EBSI, DNS)<br> -Special business logic / flows (e.g. based on EBSI/ESSIF specifications<br> -Integration with 3rd party SSI components<br> -Configuration management | |
