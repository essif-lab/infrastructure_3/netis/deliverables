# Envisioned Interoperability with Others

### eSSIF-Lab cooperations

Already established connection: 

- **iGrant**: already exchanged deliverables; we will find common ground to include us in their paper and include them into our paper; we will try to see how we can connect their data agreements into our Authority Delegation Story; detailed topics & test for interop testing needs to be defined.

- **Visma**: already exchanged deliverables; interoperability between our syscomponents and Visma's Mandate Service.


Next steps:
- contact all IOC and BOC subgrantees that have the user agents
- create interoperability project in [Interoperability Playground](https://gitlab.grnet.gr/essif-lab/interoperability)  


### Beyond eSSIF-Lab cooperations

Already established connection: 

- **TNO/VISMA**: they are both involved in contribute to Sovrin Guardianship Working Group (SGWG ); we've already exchanged information and will make monthly update calls.

Next steps:
- connect through DIF and OIDF


