# M9 Feasibility Report ADVM

# Technical accomplishments

The project has focused around the following three work packages:

## RESEARCH

- Delegation & Mandates
- ZKPs
- Ontologies and Semantics (e.g. REGO)
- Alignment with EBSI/ESSIF

## ARCHITECTURE

- Technical Architecture
- Data Modelling
- Flows
- Interfaces/APIs

## IMPLEMENTATION

- Delegation and mandates
- Integration with OSS stack (SSI Kit and Wallet by walt.id)
- UI & Demo
- Publication of Code
- Technical documentation and [research results (paper)](Delegation___Mandates_ESSIFLabs__Infrastructure_Call__-_RESEARCH_PAPERS.pdf)


# Deliveries and Achieved Functionality

The project team successfully delivered the project as proposed. Moreover, a demo was implemented to showcase the delegation and mandate functionality.

With this project we addressed the SSI aspect of Authority Delegation (AD) or Verifiable Mandates (VM) – as referred within EBSI/ESSIF. VM are fundamental for the broad adoption of SSI by businesses (e.g. mandate employees) & individuals (e.g. parents representing children). However, there are no detailed governance frameworks and outlined process flows, which cover the VMs, particularly no solutions aligned with ESSIF as well as legacy frameworks like eIDAS. Furthermore, there are no viable tech solutions for it, which would enable simple usage.

The project delivered the following: 

**(1) [research paper](Delegation___Mandates_ESSIFLabs__Infrastructure_Call__-_RESEARCH_PAPERS.pdf) on VM;   

(2) tech specs, including     

(2.1) data models and schemas for VMs,    

(2.2) Flows for issuance, verification and revocation based on eIDAS levels of assurances and leveraging ESSIF trusted registries,     

(3) Implementation of an open-source solution (SDK/lib in Java/Kotlin) incl. a user interface,     

(4) wallet integrations and     

(5) demo.**


## Credential Definition for Delegation and Mandates

According to the research paper, the following EBSI/ESSIF compliant W3C Verifiable Credential represents the data model of a Verifiable Mandate. Note that the credential is of type “VerifiableMandate'' and contains the respective `credentailSubject` that allows the flexible definition of a grant, role and constraints to delegate permissions to another individual (holder) permissions. Furthermore that credential is signed by a JSON-LD proof by an ECDSA Secp256k1 key.

```
{
 "@context" : [ "https://www.w3.org/2018/credentials/v1" ],
 "type" : [ "VerifiableCredential", "VerifiableMandate" ],
 "credentialSubject" : {
   "holder" : {
     "constraints" : {
       "location" : "Slovenia"
     },
     "grant" : "apply_to_masters",
     "id" : "did:key:z6MknRcjR9WzvXFPibQRRc9TphtHHjdAkDDhi9m3cGDUyLtL",
     "role" : "family"
   },
   "id" : "did:ebsi:ze2dC9GezTtVSzjHVMQzpkE",
   "policySchemaURI" : ""
 },
 "evidence" : [ {
   "evidenceValue" : "",
   "id" : "https://essif.europa.eu/tsr-va/evidence/f2aeec97-fc0d-42bf-8ca7-0548192d5678",
   "type" : [ "VerifiableMandatePresentation" ]
 } ],
 "id" : "urn:uuid:92f43fe2-aa37-474e-bdc7-790aee6ea034",
 "issued" : "2022-05-19T19:13:39.267078786Z",
 "issuer" : "did:ebsi:ze2dC9GezTtVSzjHVMQzpkE",
 "validFrom" : "2022-05-19T19:13:39.267080536Z",
 "issuanceDate" : "2022-05-19T19:13:39.267080536Z",
 "credentialSchema" : {
   "id" : "https://api.preprod.ebsi.eu/trusted-schemas-registry/v1/schemas/0xb77f8516a965631b4f197ad54c65a9e2f9936ebfb76bae4906d33744dbcc60ba",
   "type" : "FullJsonSchemaValidator2021"
 },
 "proof" : {
   "type" : "EcdsaSecp256k1Signature2019",
   "creator" : "did:ebsi:ze2dC9GezTtVSzjHVMQzpkE",
   "created" : "2022-05-19T19:13:39Z",
   "proofPurpose" : "assertion",
   "jws" : "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFUzI1NksifQ..pt6R6DSpxFU3cPnNXOgWhcySeoZSQf0Mut1U9LJO4t7pWF2Z7uC9BIM9KAplAnAD82Ok4R6ZDpa_E7rrES-dSQ"
 }
}
```
   

In order to validate the correctness of the credential format a Json Schema for the Verifiable Mandate credential was implemented and can be found in GitHub here:
https://github.com/walt-id/waltid-ssikit-vclib/blob/master/src/test/resources/schemas/VerifiableMandate.json

The Open Source Library “vclib” where the Verifiable Mandate credential is maintained can be found here: https://github.com/walt-id/waltid-ssikit-vclib. An output of the serialized Verifiable Mandate is here:
https://github.com/walt-id/waltid-ssikit-vclib/blob/master/src/test/resources/serialized/VerifiableMandate.json

This vclib is a dependency of the SSI kit and therefore the Verifiable Mandate will be accessible in several interfaces of the SSI Kit for conveniently working with this special credential (e.g. issuing, verifying and storing). For instance it can can be retrieved in the [“template” list](https://signatory.ssikit.walt.id/v1/swagger#/Verifiable%20Credentials/listTemplates), where the VM template acts as input to the issuing process.

![VC-list-templates](/VC-list-templates.PNG "VC list templates")

## Integration with OSS stack (SSI Kit and Wallet)

This section explains the integration of the Verifiable Mandate credential in the existing open source SSI solution from walt.id. It furthermore elaborates the overall architecture of how to create and verify a Verifiable Mandate.    

The solution consists of three essential components:

1. The SSI Kit that offers the user (via CLI) or or an application (via REST API) a convenient interface to issue and verify VMs.
2. The Trusted Policy Registry for hosting policy schemas. In an ideal future scenario this will be hosted like the Trusted Schema Registry and the Trusted Issuer Registry by the European Blockchain Services Infrastructure (EBSI).
3. The Open Policy Agent, for processing verification requests. This is a third-party open source component that should be deployed under the same administrativ control (same IT infrastructure) as the SSI Kit.    


The following graphic illustrates the verification process of a Verifiable Mandate.   

![VM verification process](/VM-verification-process.PNG "VM verification process")

A user sends a signed VM to the SSI Kit (Auditor API). The auditor runs the credential verification process, which can be configured dynamically by executing a set of predefined “Policies'' e.g. SignaturePolicy, TrustedIssuerRegistryPolicy (EBSI), JsonSchemaPolicy (further policies can be reviewed by calling this webservice https://auditor.ssikit.walt.id/v1/swagger#/Verification%20Policies/listPolicies ). This solution was extended by a “VerfiableMandatePolicy”, which processes the instructions embedded in the credentialSubject from the VM credential. The RegoValidator is called in order to fetch the referred Policy (policySchemaURI) from the Trusted Policy Registry and forwards it, together with the credential data to the Open Policy Agent. The Open Policy Agent evaluates the policy and simply returns if the validation was successful or not. This result is accumulated with the results of the other validation checks the Auditor performs in order to classify the presentation of the Verifiable Mandate.

The source code repository of the Open Policy Agent is here https://github.com/open-policy-agent/opa 

SSI Kit can be found here https://github.com/walt-id/waltid-ssikit The SSI Kit is written in Kotlin and therefore can be used as dependency in any JVM (Java Virtual Machine). Furthermore it can be deployed as a microservice and accessed over its REST API. The SWAGGER documentation can be accessed here:

- https://core.ssikit.walt.id/v1/swagger
- https://custodian.ssikit.walt.id/v1/swagger
- https://signatory.ssikit.walt.id/v1/swagger 
- https://auditor.ssikit.walt.id/v1/swagger
- https://essif.ssikit.walt.id/v1/swagger 

## Wallet Integration

The walt.id SSI stack comes with an open source custodial web-wallet, which can be self-deployed and whitelabled. The UI is hosted for demo-purpose here https://wallet.walt.id and the wallet backend API can be found here https://wallet.walt.id/api/swagger 

The wallet was extended with the Verifiable Mandate Page to represent a Verifiable Mandate Credential
https://github.com/walt-id/waltid-web-wallet/blob/35a27b661114c13fd3757be34df3028a21a95495/components/credentials/VerifiableMandate.vue

## Demo Application (Issuing & Verifying Verifiable Mandates)

We have documented the demo on the following [link](https://github.com/walt-id/waltid-ssikit-docs/blob/main/usage-examples/verifiable-credentials/delegation-and-mandates.md ).

Further documentation will be hosted [here](https://docs.walt.id/usage-examples/verifiable-credentials/delegation-and-mandates).

# Interoperability and Standardization

The core of the project is mostly agnostic towards any technology stack. Putting it differently, the theoretical basis for delegation and mandates created with this proposal can be implemented in many different ways (e.g. using different blockchains, cryptographic keys and signature suits, data exchange protocols).
The technical implementation of the project delivered by the project team utilises walt.id’s SSI Kit and Wallet, both open source (Apache 2) libraries based on open standards (e.g. W3C, DIF, OIDF, ESSIF) and optimised for openness, composability and interoperability. As a result, the project benefits from standards compliance and can be used in different ecosystems (e.g. EBSI/ESSIF, Gaia-X or other ecosystems following latest W3C, DIF and OIDF standards).

# Dissemination Overview

- [Netis and Partners Receive Funding from eSSIF-Lab’s Infrastructure Oriented Call](https://netis.si/en/netis-and-partners-receive-funding-from-essif-labs-infrastructure-oriented-call/) [4. 11. 2021]

- Blockchain LAB:UM LinkedIn [Post](https://www.linkedin.com/posts/blockchain-lab-um_netis-and-partners-receive-funding-from-essif-labs-activity-6872544802504757248-t5jP?utm_source=linkedin_share&utm_medium=member_desktop_web) 

- [Research Paper Showcase](Delegation___Mandates_ESSIFLabs__Infrastructure_Call__-_RESEARCH_PAPERS.pdf)

# Summary and Conclusions

The project was successfully developed and benefits from integrations with a open source stacks that are seeing increased adoption and are aligned with the emerging EU identity/SSI standards.

Apart from interoperability and standard compliance the integration with the walt.id open source infrastructure has the benefit that developers and organisations using walt.id can benefit from the results of this project directly. In other words, the project leverages walt.id’s open source products as a platform to drive adoption of the project result. This means that public Institutions (e.g. EU Commission, EQAR), consortia and large-scale multi-country projects (EBSI/ESSIF, Gaia-X, H2020 projects), 10-15 EU governments as well as Start-ups, scale-ups and enterprises across verticals (banking and financial services, web3, education, employment, marketplaces, supply chain, …) and web3 ecosystems get easy access to delegation capabilities developed in the course of this project.

In case of additional questions or demo, please feel free to contact: sebastjan.pirih@netis.si.
